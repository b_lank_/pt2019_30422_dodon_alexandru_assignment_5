package hw5;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.BinaryOperator;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {	
	public static void main(String[] args) {
		String activitiesFileName = "/home/alex/Documents/CTI-E/year2sem2/pt/PT2019/activities.txt";
		List<MonitoredData> activities = new ArrayList<MonitoredData>();
		
		//task 1
		
		try {
			Files
			.lines(Paths.get(activitiesFileName))
			.parallel()
			.map(s -> {
				String[] tokens = s.split("[\\s]+");
				return new MonitoredData(
					LocalDateTime.parse(tokens[0]+ " "+tokens[1], DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")),
					LocalDateTime.parse(tokens[2]+ " "+tokens[3], DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")),
					tokens[4]
				);
			})
			//.peek(System.out::println)
			.forEach(i -> activities.add(i));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//task 2
		
		long daysRecorded = activities
		.stream()
		.parallel()
		.map(MonitoredData::getStartTime)
		.map(d -> d.toLocalDate())
		.distinct()
		//.peek(System.out::println)
		.count();
		
		System.out.println("\nNumber of days in log: " + daysRecorded + "\n");
	
		//task 3
		
		System.out.println("How many times each activity appeared over the monitored period:");
		activities
		.stream()
		.parallel()
		.collect(Collectors.groupingBy(MonitoredData::getActivity, Collectors.counting()))
		.forEach((String s, Long l) -> System.out.println(s + ": " + l));
		
		//task 4
		
		System.out.println("\nHow many times each activity appeared each day:");
		activities
		.stream()
		.parallel()
		.collect(Collectors.groupingBy(
				(MonitoredData a) -> a.getStartTime().toLocalDate().toString(), 
				Collectors.groupingBy(MonitoredData::getActivity, Collectors.counting())
		))
		.forEach((d, m) -> {
			System.out.println(d + ":");
			m.forEach((a, l) -> System.out.println("\t" + a + ": " + l));
			System.out.println();
		});
		
		//task 5
		
		System.out.println("\nEach entry with the duration of the activity:\n");
		activities
		.stream()
		.parallel()
		.map(a -> a.toString() + "\t\t" + (a.getDuration() < 60 ? a.getDuration() + " seconds" : 
										a.getDuration() < 3600 ? a.getDuration()/60 + " minutes " + (a.getDuration()%60 != 0 ? a.getDuration()%60 + " seconds" : "") :
										a.getDuration()/3600 + " hours " + (a.getDuration()/60 - a.getDuration()/3600 * 60 != 0 ? (a.getDuration()/60 - a.getDuration()/3600 * 60) + " minutes " : "")
										+ (a.getDuration()%60 != 0 ? a.getDuration()%60 + " seconds": ""))
		)
		.sequential()
		.sorted((a, b) -> a.compareTo(b))
		.forEach(System.out::println);
		
		//task 6
		
		System.out.println("\nTotal duration for each activity:");
		activities
		.stream()
		.parallel()
		.collect(Collectors.groupingBy(
				MonitoredData::getActivity, 
				Collectors.summingLong(i -> i.getDuration())
		))
		.forEach((a, b) -> System.out.println(a + ":\t\t" + 
				(b < 60 ? b + " seconds" : 
					b < 3600 ? b/60 + " minutes " + (b%60 != 0 ? b%60 + " seconds" : "") :
					b/3600 + " hours " + (b/60 - b/3600 * 60 != 0 ? (b/60 - b/3600 * 60) + " minutes " : "")
					+ (b%60 != 0 ? b%60 + " seconds": ""))
		));
		
		//task 7
		
		Map<String, Long> activityAndCount = 
			activities
			.stream()
			.parallel()
			.collect(Collectors.groupingBy(
					MonitoredData::getActivity,
					Collectors.counting()
			));
		
		Map<String, Long> under5 = 
			activities
			.stream()
			.parallel()
			.filter(i -> i.getDuration() < 5*60)
			.collect(Collectors.groupingBy(
					MonitoredData::getActivity,
					Collectors.counting()
			));
		
		System.out.println("\nActivities that have at least 90% of the monitoring records with duration less than 5 minutes:");
		activities
		.stream()
		.parallel()
		.filter(a -> (under5.get(a.getActivity()) == null ? 0 : under5.get(a.getActivity())) >= 0.9 * activityAndCount.get(a.getActivity()))
		.collect(Collectors.groupingBy(MonitoredData::getActivity))
		.forEach((a, b) -> System.out.println(a));
	}
}
