package hw5;

import java.time.Duration;
import java.time.LocalDateTime;

public class MonitoredData {
	private LocalDateTime startTime;	
	private LocalDateTime endTime;
	private String activity;
	public MonitoredData(LocalDateTime startTime, LocalDateTime endTime, String activity) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
		this.activity = activity;
	}
	public LocalDateTime getStartTime() {
		return startTime;
	}
	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}
	public LocalDateTime getEndTime() {
		return endTime;
	}
	public void setEndTime(LocalDateTime endTime) {
		this.endTime = endTime;
	}
	public String getActivity() {
		return activity;
	}
	public void setActivity(String activity) {
		this.activity = activity;
	}
	@Override
	public String toString() {
		return startTime + "\t" + endTime + "\t" + activity;
	}
	public long getDuration() {
		return Duration.between(this.startTime, this.endTime).toMillis() / 1000;
	}
}
